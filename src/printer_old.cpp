#include <node.h>
#if _MSC_VER
  #include <windows.h>
  #include <wingdi.h>
  #include <winuser.h>
  #include <WinSpool.h>
#endif


namespace PrintDocumentWrapper
{
  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::Object;
  using v8::String;
  using v8::Value;


  // Function prototypes
  void PrintDocument(const FunctionCallbackInfo<Value>& args);


  //--------------------------------------------------------------------------------------------------
  //                                        F U N C T I O N S
  //--------------------------------------------------------------------------------------------------


  // This function is the initialization function
  void Initialize(Local<Object> exports)
  {
    // Set the exported functions
    NODE_SET_METHOD(exports, "PrintDocument", PrintDocument);
  }

  // V8 function prototypes
  NODE_MODULE(NODE_GYP_MODULE_NAME, Initialize)


  //--------------------------------------------------------------------------------------------------


  // This function prints the supplied text and can do a form feed
  // args[0] = String  : name of the printer
  // args[1] = String  : text to be printed
  // args[2] = boolean : should we end with a form feed?
  void PrintDocument(const FunctionCallbackInfo<Value>& args)
  {
    HDC      hDcPrinter;
    int      iResult = -1;
    Isolate* isolate = nullptr;

    // Get the arguments
    args.GetIsolate();

    // Do we have 3 valid parameters?
    if(args.Length() == 3  &&  args[0]->IsString()  &&  args[1]->IsString()  &&  args[2]->IsBoolean())
    {
      // Get the printer's name
      v8::String::Utf8Value strValuePrinter(args[0]->ToString());
      std::string strPrinter;
      strPrinter.assign(*strValuePrinter, strValuePrinter.length());

      // Get the context of the printer
      if((hDcPrinter = CreateDC(NULL, strPrinter.c_str(), NULL, NULL)) != nullptr)
      {
        DOCINFO docinfo = {};

        // Initialize the docinfo object
        docinfo.cbSize = sizeof(docinfo);

        // Start the document
        if(StartDoc(hDcPrinter, &docinfo) > 0)
        {
          RECT rcPage;

          // Get the page size
          rcPage.left   = GetDeviceCaps(hDcPrinter, PHYSICALOFFSETX);
          rcPage.top    = GetDeviceCaps(hDcPrinter, PHYSICALOFFSETY);
          rcPage.right  = GetDeviceCaps(hDcPrinter, HORZRES) - GetDeviceCaps(hDcPrinter, PHYSICALOFFSETX);
          rcPage.bottom = GetDeviceCaps(hDcPrinter, VERTRES) - GetDeviceCaps(hDcPrinter, PHYSICALOFFSETY);

          // Start a new page
          if(StartPage(hDcPrinter) > 0)
          {
            // Get the text
            v8::String::Utf8Value strTextPrinter(args[1]->ToString());
            strPrinter.assign(*strTextPrinter, strTextPrinter.length());

            // Print the text
            iResult = DrawText(hDcPrinter, strPrinter.c_str(), strPrinter.length(), &rcPage, DT_TOP | DT_LEFT | DT_NOPREFIX);

            // End the page
            EndPage(hDcPrinter);
          }

          // Do we need to send a form feed?
          if(args[2]->IsTrue())
          {
            // Do a form feed by sending a blank page
            if(StartPage(hDcPrinter) > 0)
              EndPage(hDcPrinter);
          }

          // End the document
          EndDoc(hDcPrinter);
        }

        // Free the resources
        DeleteDC(hDcPrinter);
      }
    }

    // Set the result
    args.GetReturnValue().Set(iResult);
  }
}
