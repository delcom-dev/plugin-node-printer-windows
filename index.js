let printer_helper;
if (process.env.DEBUG) {
  printer_helper= require('./build/Debug/node_printer_win.node')
} else {
  printer_helper= require('./build/Release/node_printer_win.node')
}

module.exports.print = print;

function print(printerName, text, formfeedNeeded = false){
	return printer_helper.PrintDocument(printerName, text, formfeedNeeded);
}